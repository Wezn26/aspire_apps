const menuFive = document.querySelector('.menuFive');
  function addClassFunFive() {
          this.classList.toggle("clickMenuFive");
      }
  menuFive.addEventListener('click', addClassFunFive);

//MAIN SLIDER START
let MainSlider = (function () {
  function shiftLeft() {
      const boxes = document.querySelectorAll(".box");
      const tmpNode = boxes[0];
      boxes[0].className = "box move-out-from-left";

      setTimeout(function() {
          if (boxes.length > 5) {
              tmpNode.classList.add("box--hide");
              boxes[5].className = "box move-to-position5-from-left";
          }
          boxes[1].className = "box move-to-position1-from-left";
          boxes[2].className = "box move-to-position2-from-left";
          boxes[3].className = "box move-to-position3-from-left";
          boxes[4].className = "box move-to-position4-from-left";
          boxes[0].remove();

          document.querySelector(".cards__container").appendChild(tmpNode);

      }, 500);

  }

  function shiftRight() {
      const boxes = document.querySelectorAll(".box");
      boxes[4].className = "box move-out-from-right";
      setTimeout(function() {
          const noOfCards = boxes.length;
          if (noOfCards > 4) {
              boxes[4].className = "box box--hide";
          }

          const tmpNode = boxes[noOfCards - 1];
          tmpNode.classList.remove("box--hide");
          boxes[noOfCards - 1].remove();
          let parentObj = document.querySelector(".cards__container");
          parentObj.insertBefore(tmpNode, parentObj.firstChild);
          tmpNode.className = "box move-to-position1-from-right";
          boxes[0].className = "box move-to-position2-from-right";
          boxes[1].className = "box move-to-position3-from-right";
          boxes[2].className = "box move-to-position4-from-right";
          boxes[3].className = "box move-to-position5-from-right";
      }, 500);

  }

  setInterval( shiftRight,5000 );

  return {
    shiftLeft: function () {
      shiftLeft();
    },
    shiftRight: function () {
      shiftRight();
    }
  };
})();


//MAIN SLIDER END

//OPACITY SLIDER START
var OpacitySlider = (function () {
    var slideIndex = 1;
    showDivs(slideIndex);

    function plusDivs(n) {
      showDivs(slideIndex += n);
    }

    function currentDiv(n) {
      showDivs(slideIndex = n);
    }

    function showDivs(n) {
      var i;
      var x = document.getElementsByClassName("mySlides");
      var dots = document.getElementsByClassName("demo");
      if (n > x.length) {slideIndex = 1;}
      if (n < 1) {slideIndex = x.length;}
      for (i = 0; i < x.length; i++) {
         x[i].style.opacity = "0";
      }
      for (i = 0; i < dots.length; i++) {
         dots[i].className = dots[i].className.replace(" w3-red", "");
      }
      x[slideIndex-1].style.opacity = "1";
      dots[slideIndex-1].className += " w3-red";
    }

    function next() {
      plusDivs(1);
    }

    setInterval( next, 4000 );

    return {
      currentDiv: function (n) {
        currentDiv(n);
      },
      plusDivs: function (n) {
        plusDivs(n);
      }
    };

  })();
//OPACITY SLIDER END

//SECTION CONTACT US START
let Contact = (function () {
  let label = document.getElementById('label'),
      label2 = document.getElementById('label2'),
      label3 = document.getElementById('label3'),
      label4 = document.getElementById('label4'),
      div = document.getElementsByClassName('list-header')[0],
      div2 = document.getElementsByClassName('list-header')[1],
      div3 = document.getElementsByClassName('list-header')[2],
      div4 = document.getElementsByClassName('list-header')[3];

      // const wp = #fff;


   function click(label, div) {
     label.addEventListener('click', function () {
       label.innerHTML = (label.innerHTML == '-') ? '+' : '-';
       label.style.backgroundColor = (label.style.backgroundColor == 'dodgerblue') ? 'white' : 'dodgerblue';
       label.style.color = (label.style.color == 'white') ? 'dodgerblue' : 'white';
       div.style.backgroundColor = (div.style.backgroundColor === 'white') ? '#2196f3' : 'white';
       });
   }

   click(label, div);
   click(label2, div2);
   click(label3, div3);
   click(label4, div4);

   return {
     click: function (label ,div) {
       click(label, div);
     }
   };

})();
Contact.click(label, div);
Contact.click(label2, div2);
Contact.click(label3, div3);
Contact.click(label4, div4);

//SECTION CONTACT US END
